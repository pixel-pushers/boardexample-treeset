package Controller;


import Model.Board;

/**
 * GAME CLASS PLACEHOLDER
 * ======================
 *
 *  this Class interacts with the model and updates the view
 *  any user input is handles by this class and others in its package
 *
 */

public class Controller implements Runnable {

    Board firstBoard= new Board();

    public void run() {
        System.out.println("Hello Snakes and Ladders");
        firstBoard.printAllLocations();
    }



}
