import Controller.Controller;

/**
 * Main Class runs controller
 */
public class Main {
    public static void main(String[ ] args) {


        Thread gameThread = new Thread(new Controller());
        gameThread.start();
    }
}
