package Model;

import java.util.Iterator;
import java.util.TreeSet;

/***************************
 *  BOARD PLACEHOLDER
 * --------------------
 *
 * This is an example of using a treeset to map the boards locations 1 to 100
 * An iterator can be created which can cycle up and down the treeset
 * ( the same as moving a piece up and down the numbered board )
 * see https://examples.javacodegeeks.com/core-java/util/treeset/java-util-treeset-example/
 * for more details about how treeset can be more beneficial than a simple array.
 *
 ***************************/

/*
 This is part of the treeset board example and can be built on top of, rewritten or deleted as needed
 */

public class Board {

    private TreeSet boardSet;

    public Board() {
        boardSet = new TreeSet<>();
        for  (int i = 1; i<101; i++) {
            // add 100 LocationExampleTreeset objects to a tree set
            boardSet.add(new Location(i));
        }

    }

    public void printAllLocations() {
        Iterator<Integer> boardLocation = boardSet.iterator();
        while(boardLocation.hasNext()){
            System.out.println("LocationExampleTreeset: "+boardLocation.next()+" mapped");
        }
}
}
