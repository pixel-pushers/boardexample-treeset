package Model;

/**
 This is part of the treeset example and can be built on top of, rewritten or deleted as needed
 */
public class Location implements Comparable {

    private Integer number;
    private Integer moveTo;

    public Location(Integer number) {
        super();
        this.number = number;
        this.moveTo = null; //null pointer issues will arise
    }

    public Location(Integer number, Integer moveTo) {
        super();
        this.number = number;
        this.moveTo = moveTo;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Integer getMoveTo() {
        return moveTo;
    }

    public void setMoveTo(Integer moveTo) {
        this.moveTo = moveTo;
    }


    @Override
    public String toString() {
        return "number " + number;
    }

        @Override
    public int compareTo(Object o) {
        Location l = (Location)o;
        return this.number.compareTo(l.number);
    }


}
